<!doctype html>
<html lang="fr">
  <head>
    <title>TPW101</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
  </head>
  <body>
    <div id="page">
      <div id="header">
	<h2>Intra lab[CQ]</h2>
	<p>Cet intra est destiné aux membre du lab[CQ].<br />
	  Vous trouverez ici cours, projets, trombi, planning, ainsi que des infos diverses.</p>
	<p>
	  <?php
   	    if (!empty($_SESSION['login']) && !empty($_SESSION['password']))
	      echo '<span id="logout">Logged with ' . $_SESSION['login'] . ' <a href="?p=logout.php">Logout</a></span>';
   	    ?>
	</p>
      </div>
