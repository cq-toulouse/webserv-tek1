<?php
ob_start();
session_start();

include("config.php");

mysql_connect($sql_host, $sql_user, $sql_password) or die("Can't connect to sql server");
mysql_select_db($sql_database) or die("Can't select db");

include("function.php");

include("header.php");

include("menu.php");

if (!empty($_GET["p"]))
  $page = "pages/" . $_GET["p"];
else
  $page = "pages/home.php";

if (file_exists($page)) {
  include($page);
}
else
  include("errors/404.php");

include("footer.php");
ob_end_flush();
?>
