<?php
  session_unset();
  session_destroy();
  ob_end_clean();
  header("Location: ?p=home.php");
?>

<div id="content">
  <p>You are now logged out</p>
</div>
