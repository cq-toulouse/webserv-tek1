
<?php
  $pages = array();
  $isAdmin = is_admin();
  $isLogged = is_logged();

  $only_admin = array("addUser.php");
  $only_logged = array("logout.php", "profil.php", "cours.php", "planning.php", "trombi.php", "home.php");

  $p = opendir("pages");
  while ($r = readdir($p)) {
      $file_info = pathinfo($r);
      if ($file_info['extension'] == "php" and (!in_array($r, $only_admin) or $isAdmin) and (!in_array($r, $only_logged) or $isLogged))
	array_push($pages, $r);
    }
?>

<div id="menu">
  <ul>
<?php
foreach ($pages as $p) {
    echo "\t\t<li><a href='?p=$p'>$p</a></li>\n";
}
?>
  </ul>
</div>

