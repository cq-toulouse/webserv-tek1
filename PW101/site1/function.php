<?php

function is_admin() {
  if (!empty($_SESSION['login']) and $_SESSION['login'] == "admin")
    return True;
  return False;
}

function is_logged($name = False) {
  if (empty($_SESSION['login']))
    return False;
  if ($name and $name != $_SESSION['login'])
    return False;
  return True;
}

?>